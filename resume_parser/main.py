from llama_cpp import Llama
llm = Llama(
    # chat model
    # model_path="/home/n4d/dev/llm-models/llama-2-7b-chat.Q5_K_S.gguf",
    # base
    # model_path="/home/n4d/dev/llm-models/llama-2-7b.Q5_K_M.gguf",
    # 13b base
    model_path="/home/n4d/dev/llm-models/llama-2-13b.Q5_K_M.gguf",
    n_ctx=4096
)

with open('data/cv_list/js.txt', 'r') as file:
    text = file.read()
    text_input = f'''## Task
A job application of an applicant is presented in brackets:
[{text}].

Summarize the applicant work experience for each workplace with next structure:
```
===
1. Employer Name (Example: "Sberbank-Technologies").
2. Employer line of business, if specified (Example: "Information Technologies").
3. Job summary in 30-50 words.
4. The applicant position or Role (Example: "Senior Frontend Developer").
5. Job duration in months, if specified (Example: "12 months").
6. Comma-separated list of specific terms, technologies and skills (Example: "Micro Frontends, React, TypeScript").
===
```
And so on. Each summary should be divided and decorated by `===` symbols.

## Solution
===
1'''
    output = llm(
        text_input,
        max_tokens=None,
        echo=True,
    )
    print(output)
